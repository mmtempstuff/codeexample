#pragma once

#include <iostream>


#define INVALID_TRANSITION(event)                                                                                      \
    virtual void On##event(const json&)                                                                                \
    {                                                                                                                  \
        std::ostringstream oss;                                                                                        \
        oss << "Invalid transition: " << name_ << " --> event(" << #event << ")\n";                                    \
        std::cout << oss.str();                                                                                        \
    }

namespace kis
{
    class HierarchicalStateMachine
    {
    private:
        ///////////////////////// base state //////////////////////////////////////////////////////

        class State
        {
        public:
            State(HierarchicalStateMachine* machine) : machine_(machine) {}
            std::string_view GetName();

            virtual void OnEnter();
            virtual void OnExit();

            INVALID_TRANSITION(Stop)
            INVALID_TRANSITION(Start)
            INVALID_TRANSITION(Next)
            INVALID_TRANSITION(Repeat)
            INVALID_TRANSITION(Pause)
            INVALID_TRANSITION(Continue)
            INVALID_TRANSITION(Status)
            INVALID_TRANSITION(StepDone)
            INVALID_TRANSITION(Error)
            INVALID_TRANSITION(Done)

        protected:
            HierarchicalStateMachine* machine_;
            std::string name_;
        };

        ///////////////////////// composite states ////////////////////////////////////////////////

        class ResetableState : public State
        {
        public:
            ResetableState(HierarchicalStateMachine* machine) : State(machine) {}

            // virtual void OnReset(const json& msg);
            virtual void OnNotify(const json& msg);
        };

        class StoppableState : public ResetableState
        {
        public:
            StoppableState(HierarchicalStateMachine* machine) : ResetableState(machine) {}

            virtual void OnStop(const json& msg);
        };

        ///////////////////////// single states ///////////////////////////////////////////////////

        class IdleState : public State
        {
        public:
            IdleState(HierarchicalStateMachine* machine) : State(machine)
            {
                name_ = "IDLE";
            }

            void OnYaml(const json& msg);
        };

        class ReadyState : public ResetableState
        {
        public:
            ReadyState(HierarchicalStateMachine* machine) : ResetableState(machine)
            {
                name_ = "READY";
            }

            void OnStart(const json& msg);
            void OnNext(const json& msg);
        };

        class RunningState : public StoppableState
        {
        public:
            RunningState(HierarchicalStateMachine* machine) : StoppableState(machine)
            {
                name_ = "RUNNING";
            }

            void OnPause(const json& msg);
            void OnStepDone(const json& msg);
            void OnError(const json& msg);
            void OnDone(const json& msg);
        };

        class ErrorState : public StoppableState
        {
        public:
            ErrorState(HierarchicalStateMachine* machine) : StoppableState(machine)
            {
                name_ = "ERROR";
            }

            void OnRepeat(const json& msg);
            void OnDone(const json& msg);
        };

        class PausedState : public StoppableState
        {
        public:
            PausedState(HierarchicalStateMachine* machine) : StoppableState(machine)
            {
                name_ = "PAUSED";
            }

            void OnRepeat(const json& msg);
            void OnNext(const json& msg);
            void OnStepDone(const json& msg);
            void OnContinue(const json& msg);
            void OnDone(const json& msg);
        };

        ///////////////////////// state handling //////////////////////////////////////////////////

        IdleState idleState_;
        ReadyState readyState_;
        RunningState runningState_;
        ErrorState errorState_;
        PausedState pausedState_;

        State* currentState_;

        void TransitionToState_(State* state);

    public:
        HierarchicalStateMachine();

        void OnCommand(const json& msg);
    };
} // namespace kis
