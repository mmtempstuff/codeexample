#include "hierarchical_state_machine.hpp"

namespace kis
{
    using namespace nlohmann;

    HierarchicalStateMachine::HierarchicalStateMachine() :
        idleState_(this), readyState_(this), runningState_(this), pausedState_(this), errorState_(this)
    {
        currentState_ = &idleState_;
        currentState_->OnEnter();
    }

    void HierarchicalStateMachine::OnCommand(const json& msg)
    {
        try
        {
            std::string name(msg.at("name").get<std::string>());

            if(name == "Start")
                currentState_->OnStart(msg);
            else if(name == "Stop")
                currentState_->OnStop(msg);
            else if(name == "Continue")
                currentState_->OnContinue(msg);
            else if(name == "Pause")
                currentState_->OnPause(msg);
            else if(name == "Next")
                currentState_->OnNext(msg);
            else if(name == "Repeat")
                currentState_->OnRepeat(msg);
            else if(name == "Status")
                currentState_->OnStatus(msg);
            else
            {
                throw std::runtime_error(std::string("Received an unknown command: ") + name);
            }
        }
        catch(const std::runtime_error& e)
        {
            std::cout << "OnCommand has caught runtime error:\n" << e.what();
            throw;
        }
    }

    void HierarchicalStateMachine::TransitionToState_(State* state)
    {
        currentState_->OnExit();

        nlohmann::json msg;
        msg["from"] = currentState_->GetName();

        currentState_ = state;

        msg["to"] = currentState_->GetName();

        communication_->SendData("StateTransition", true, msg);

        currentState_->OnEnter();
    }

    std::string_view HierarchicalStateMachine::State::GetName()
    {
        return name_;
    }

    void HierarchicalStateMachine::State::OnEnter()
    {
        std::cout << "Entered " << name_ << " state.\n";
    }

    void HierarchicalStateMachine::State::OnExit()
    {
        std::cout << "Exiting " << name_ << " state.\n";
    }

    ///////////////////////// composite states ////////////////////////////////////////////////////

    void HierarchicalStateMachine::StoppableState::OnStop(const json& msg)
    {
        std::cout << "Handling OnStop\n";
    }

    ///////////////////////// single states ///////////////////////////////////////////////////////

    void HierarchicalStateMachine::ReadyState::OnStart(const json& msg)
    {
        std::cout << "Handling OnStart\n";
    }

    void HierarchicalStateMachine::ReadyState::OnNext(const json& msg)
    {
        std::cout << "Handling OnNext\n";
    }

    void HierarchicalStateMachine::RunningState::OnPause(const json& msg)
    {
        std::cout << "Handling OnPause\n";
    }

    void HierarchicalStateMachine::RunningState::OnStepDone(const json& msg)
    {
        std::cout << "Handling OnStepDone\n";
    }

    void HierarchicalStateMachine::RunningState::OnDone(const json& msg)
    {
        std::cout << "Handling OnDone\n";
    }

    void HierarchicalStateMachine::RunningState::OnError(const json& msg)
    {
        std::cout << "Handling OnError\n";
    }

    void HierarchicalStateMachine::ErrorState::OnRepeat(const json& msg)
    {
        std::cout << "Handling OnRepeat\n";
    }

    void HierarchicalStateMachine::ErrorState::OnDone(const json& msg)
    {
        std::cout << "Handling OnDone\n";
    }

    void HierarchicalStateMachine::PausedState::OnRepeat(const json& msg)
    {
        std::cout << "Handling OnRepeat\n";
    }

    void HierarchicalStateMachine::PausedState::OnNext(const json& msg)
    {
        std::cout << "Handling OnNext\n";
    }

    void HierarchicalStateMachine::PausedState::OnContinue(const json& msg)
    {
        std::cout << "Handling OnContinue\n";
    }

    void HierarchicalStateMachine::PausedState::OnStepDone(const json& msg)
    {
        std::cout << "Handling OnStepDone\n";
    }

    void HierarchicalStateMachine::PausedState::OnDone(const json& msg)
    {
        std::cout << "Handling OnDone\n";
    }

    void HierarchicalStateMachine::PausedState::OnError(const json& msg)
    {
        std::cout << "Handling OnError\n";
    }
} // namespace kis
